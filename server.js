const express = require('express'); //referencia al paquete express
const bodyParser = require('body-parser');
const userFile = require('./user.json');
var app = express(); // creamos el servidor de Node
app.use(bodyParser.json()); // usamos el body-parser
var puerto = process.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';

// GEt a todos los usuarios: user.json
app.get (URL_BASE + 'users',
function(request, response) {
//  response.send('Hola Universo');
  response.status(200);
  response.send(userFile);
});

// Peticion GET con Query String
app.get (URL_BASE + 'usersq',
function(request, response) {
  var ind=1;
  var arr = [];
  let objQuery = request.query.max;
  console.log(objQuery);
while (ind <= objQuery) {
  arr.push(userFile[ind-1]);
  ind=ind+1;
}
  response.status(200);
  response.send(arr);
});

// operacion POST a users
app.post (URL_BASE + 'users',
function(req, res) {
  console.log(req.body);
  let lon = userFile.length;
  console.log(lon);
  let nuevoUsu = {
    id: ++lon,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
  }
  userFile.push(nuevoUsu);
  res.status(201).send({"mensaje":"usuario creado correctamente","Actualizado": userFile});
});

app.delete (URL_BASE + 'users/:id',
function(req, res) {
  let indice = req.params.id;
  let estado = (userFile[indice-1]== undefined) ? 0:1;
  if(estado==0) {
    let respuesta = {"msg": "No existe"};
    console.log(respuesta);
    res.status(201).send(respuesta);
    }
  else{
    let respuesta = {"Cliente Eliminado": userFile.splice(indice-1,1)};
    console.log(respuesta);
    res.status(201).send({"mensaje":"Cliente Eliminado correctamente","Actualizado": userFile});
   }
});

// PUT a users. Luis Ormeño Cáceres. AQUI ESTA LA SEGUNDA TAREA %%%%%%%%%%%%%%
// PUT a users. Luis Ormeño Cáceres. AQUI ESTA LA SEGUNDA TAREA %%%%%%%%%%%%%%
// Por ahora se actualiza también el "Id", sin embargo, podría dejar de actualizarse.
app.put (URL_BASE + 'users/:id',
function(req, res) {
  let indice = req.params.id;
  console.log(req.body);
  console.log(indice);
  let userFileUpd = (userFile[indice-1]== undefined) ?
                     {"msg": "Cliente No existe"}:userFile[indice-1];
  console.log(userFileUpd);
  let updUsu = {
    id: req.body.id,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
  };
  userFileUpd = updUsu;
  userFile[indice-1] = userFileUpd;
  res.status(201).send({"mensaje":"usuario Actualizado correctamente","Actualizado": userFile});
});

app.get (URL_BASE + 'users/:id',
function(request, response) {
  let indice = request.params.id;
  let respuesta = (userFile[indice-1]== undefined) ? {"msg": "No existe"}:userFile[indice-1];
  console.log(respuesta);
  response.status(200).send(respuesta);
});

app.get (URL_BASE + 'userstotal',
function(request, response) {
  let lon = userFile.length;
  let lonJson = JSON.stringify({num_elem: lon});
//  console.log(lon);
  response.status(200).send(lonJson);
});

// http://localhost:3000/fred
app.listen(puerto,function(){
  console.log('Node JS escuchando puerto: 3000');
});
